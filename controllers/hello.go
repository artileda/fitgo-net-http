package controllers

import (
	"fitgo/views"
	"net/http"
)

func Hello(w http.ResponseWriter, r *http.Request) {
	var i interface{}
	v := views.View{Response: w, Path: "index", Data: i, Template: "application"}
	views.Render(v)
}

func ProdukIndex(w http.ResponseWriter, r *http.Request) {
	var i interface{}
	v := views.View{Response: w, Path: "produk/index", Data: i, Template: "application"}
	views.Render(v)
}

func FormIndex(w http.ResponseWriter, r *http.Request) {

}
