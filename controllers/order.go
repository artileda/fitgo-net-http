package controllers

import (
	"context"
	"encoding/json"
	"fitgo/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"net/http"
)

// https://medium.com/glottery/golang-and-mongodb-with-go-mongo-driver-part-1-1c43aba25a1
// https://medium.com/@faygun89/create-rest-api-with-golang-and-mongodb-d38d2e1d9714

type Item struct{
	Nama string
	Harga float64
	Jumlah int
	// disini kalau mau nambah tender
}
type Order struct {
	ID primitive.ObjectID
	Status string
	Keranjang []Item
	DenganNama models.User
}

func BuatKeranjang(w http.ResponseWriter,r *http.Request){
	keranjang := Order{}
	err := json.NewDecoder(r.Body).Decode(&keranjang)
	if err != nil {

	}

	response , err := models.DB.Collection("order").InsertOne(context.Background(),keranjang)
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Print(response)
}

func IsiKeranjang(w http.ResponseWriter,r *http.Request){

   //_, err = models.DB.Collection("order").FindOne(context.Background(),)
}