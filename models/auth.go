package models

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	//"github.com/casbin/casbin"
	"golang.org/x/crypto/bcrypt"
	"log"
)

type User struct {
	ID primitive.ObjectID `json:"id,omitempty" bson:"id,omitempty"`
	Username string  `json:"username,omitempty" bson:"username,omitempty"`
	Password string  `json:"password" bson:"password,omitempty"`
	Role string `json:"omitempty" bson:"role,omitempty"`
}

func (u User) Find(id string)  bool{
	result := DB.Collection("auth").FindOne(context.Background(),bson.D{{"id", id}})
	return result != nil
}

func (u User) Validate () bool {
	current := true
	usernameLength := len(u.Username)
	if !(usernameLength > 8) {
		current = false
	}
	passLenght := len(u.Password)
	if !(passLenght > 8){
		current = false
	}
	return current
}

func (u User) Save () bool {
	result,err := DB.Collection("auth").InsertOne(context.TODO(),u);
	if err != nil{
		log.Print(err)
		return false
	}else {
		log.Println(result.InsertedID)
		return true
	}
}

func (u User) Auth() bool {
	result := DB.Collection("auth").FindOne(context.Background(),bson.D{{"username",u}})
	userStruct := User{}
	result.Decode(&userStruct)
	if err := bcrypt.CompareHashAndPassword([]byte(userStruct.Password),[]byte(u.Password)); err == nil {
		return true
	}else{
		return false
	}
}

