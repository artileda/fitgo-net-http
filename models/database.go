package models

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
)

var Mongod *mongo.Client
var DB *mongo.Database

func InitMongo() {
	mongoc, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost"))
	if err != nil {
		return
	}

	err = mongoc.Connect(context.Background())
	if err != nil {
		log.Fatal("[!] Database failed connect.")
	}

	err = mongoc.Ping(context.Background(), readpref.Primary())
	if err != nil {
		log.Fatal("[!] Database Gone!")
	}

	Mongod = mongoc
	DB = Mongod.Database("fitgo")
	log.Print("[*] Database Connected!")
}
