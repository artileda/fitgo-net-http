package models

import (
	"encoding/json"
	"errors"
	"github.com/casbin/casbin"
	"github.com/gomodule/redigo/redis"
	"log"
	"net/http"
)


var redisSession redis.Conn

type Access struct {

}

func CreateSession(u User){
	user,_ := json.Marshal(u)
	response ,err := redisSession.Do("SETEX",user)
	if err != nil{

	}
	log.Print(response)
}

func FindSessionAndAuthorize(e *casbin.Enforcer) func(next http.Handler) http.Handler{
	return func(next http.Handler) http.Handler {
		res := func(w http.ResponseWriter, r *http.Request) {
			var sessionKey string
			cookies, err := r.Cookie("session")
			if err != nil {
				if err == http.ErrNoCookie {

				}
			} else {
				sessionKey = cookies.Value
			}
			var role string;
			if cookies == nil && err == http.ErrNoCookie {
				role = "anonymous"
			} else {
				response, err := redis.String(redisSession.Do("GET", sessionKey))
				if err != nil {
					log.Print(err)
				}
				current_user := User{}
				err = json.Unmarshal([]byte(response), current_user)
				if err != nil {
					log.Print(err)
				}
				role = current_user.Role
			}
			// Set to Role Checker

			allow, err := e.EnforceSafe(role, r.URL.Path, r.Method)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
			}
			log.Print(role)
			log.Print(allow)
			if allow {
				next.ServeHTTP(w,r)
			}else{
				log.Print("Denied")
				writeError(http.StatusForbidden,"forbiden",w,errors.New("unauthorized"))
				return
			}
		}
		return http.HandlerFunc(res)
	}
}

func writeError(status int, message string, w http.ResponseWriter, err error) {
	log.Print("ERROR: ", err.Error())
	w.WriteHeader(status)
	w.Write([]byte(message))
}
