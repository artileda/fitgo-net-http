package fitgo

import (
	"fitgo/models"
	"github.com/casbin/casbin"
	"log"
	"mime"
	"net/http"
	"path/filepath"

	"fitgo/controllers"
	"github.com/gorilla/mux"
)

var current_user models.User =  models.User{}
var router *mux.Router = mux.NewRouter()

type Response struct {
	Msg    string
	Status int
}

// Router
func routes() {

	route("/", controllers.Hello, "GET")
	//route("/set", func(w http.ResponseWriter, r *http.Request) {
	//	current_user = models.User{primitive.NewObjectID(),"UwU","OvO"}
	//}, "GET")
	//route("/cek", func(w http.ResponseWriter, r *http.Request) {
	//	log.Println(current_user)
	//}, "GET")

	route("/produk", controllers.ProdukIndex, "GET")
	//route("/produk",)

	route("/api/produk/{id}", controllers.ShowProduk, "GET")
	route("/api/produk", controllers.AddProduk, "POST")
	route("/api/produk", controllers.IndexProduk, "GET")
	route("/api/produk/{id}", controllers.DelProduk, "DELETE")
	route("/api/produk/{id}", controllers.UpdateProduk, "PUT")


}

//
//func Params(r *http.Request) map[string]string {
//	return mux.Vars(r)
//}

// https://www.alexedwards.net/blog/making-and-using-middleware


func route(u string, cb func(w http.ResponseWriter, r *http.Request), m string) {
	router.HandleFunc(u, cb).Methods(m)
}

func StaticServe(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	mimes := vars["mime"]
	file := vars["file"]
	filePath := "./assets/" + mimes + "/" + file
	log.Println(filePath)
	w.Header().Set("Content-Type", mime.TypeByExtension(filepath.Ext(filePath)))
	http.ServeFile(w, r, filePath)
}
func Run() {

	router.HandleFunc("/assets/{mime}/{file}", StaticServe)
	routes()
	auth,err := casbin.NewEnforcerSafe("config/auth.conf","config/policy.csv")
	if err != nil {
		log.Fatal(err)
	}
	log.Print("HTTP Services Started!")

	http.ListenAndServe(":8080", models.FindSessionAndAuthorize(auth)(router))
}
