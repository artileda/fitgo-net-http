package main

import (
	"fitgo"
	"fitgo/models"
)

func main() {
	models.InitMongo()
	fitgo.Run()
}
